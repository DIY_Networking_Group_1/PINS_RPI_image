# Install MQTT lib:
on_chroot << EOF
git clone https://github.com/eclipse/paho.mqtt.c.git
cd paho.mqtt.c
make
make install
cd ..
EOF

# Install PINS-BM:
on_chroot << EOF
git clone https://gitlab.com/DIY_Networking_Group_1/PINS-BM.git
cd PINS-BM/
make init
make compile
make install
rm -rf ../PINS-BM/
chown pinner /opt/pins
systemctl daemon-reload
systemctl enable pinns.service
EOF

# Install PINS-BM service:
install -m 644 files/pinns.service "${ROOTFS_DIR}/etc/systemd/system/"