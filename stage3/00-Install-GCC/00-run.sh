on_chroot << EOF
apt remove g++ -y
apt autoremove -y
git clone https://bitbucket.org/sol_prog/raspberry-pi-gcc-binary.git
cd raspberry-pi-gcc-binary
tar xf gcc-8.1.0.tar.bz2
mv gcc-8.1.0 /usr/local
echo 'export PATH=/usr/local/gcc-8.1.0/bin:$PATH' >> /home/pinner/.bashrc.bashrc
source /home/pinner/.bashrc
rm ../raspberry-pi-gcc-binary -rf
ln /usr/local/gcc-8.1.0/bin/g++-8.1.0 /usr/bin/g++ -s
EOF