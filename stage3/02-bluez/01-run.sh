on_chroot << EOF
wget http://www.kernel.org/pub/linux/bluetooth/bluez-5.50.tar.xz
tar xvf bluez-5.50.tar.xz
cd bluez-5.50
./configure
make
make install
systemctl start bluetooth
systemctl enable bluetooth
EOF
