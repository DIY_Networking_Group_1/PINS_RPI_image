if [ -f config ]; then
   source config
fi

if [ -z "${IMG_NAME}" ]; then
   echo "IMG_NAME not set" 1>&2
   exit 1
fi
BASE_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
export IMG_DATE="${IMG_DATE:-"$(date +%Y-%m-%d)"}"
mkdir build
export WORK_DIR="${WORK_DIR:-"${BASE_DIR}/work/${IMG_DATE}-${IMG_NAME}"}"
mv ${WORK_DIR}/export-image/${IMG_DATE}-${IMG_NAME}-lite.img build/
